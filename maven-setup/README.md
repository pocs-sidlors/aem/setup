## setup maven

### custom adobe settings.xml to M2

Movemos settings.xml a $HOME/.m2

Validamos en la terminal

```xml
$ mvn help:effective-settings
    ...

<activeProfiles >
    <activeProfile>adobe-public</activeProfile>
</activeProfiles>
<pluginGroups>
    <pluginGroup>org.apache.maven.plugins</pluginGroup>
    <pluginGroup>org.codehaus.mojo</pluginGroup>
</pluginGroups>
</settings>
[INFO] -----------------------------------
[INFO] BUILD SUCCESS
[INFO] -----------------------------------
[INFO] Total time:  0.856 s
```

### Generacion de proyectos por medio de arquetipos

Para crear un proyecto usando el archetype de adobe


```sh
mvn archetype:generate -B \
    -DarchetypeGroupId=com.adobe.granite.archetypes \
    -DarchetypeArtifactId=aem-project-archetype \
    -DarchetypeVersion=22 \
    -DgroupId=com.adobe.aem.guides \
    -Dversion=0.0.1-SNAPSHOT \
    -DappsFolderName=wknd \
    -DartifactId=aem-guides-wknd \
    -Dpackage=com.adobe.aem.guides.wknd \
    -DartifactName="WKND Sites Project" \
    -DcomponentGroupName=WKND \
    -DconfFolderName=wknd \
    -DcontentFolderName=wknd \
    -DcssId=wknd \
    -DisSingleCountryWebsite=n \
    -Dlanguage_country=en_us \
    -DoptionAemVersion=6.5.0 \
    -DoptionDispatcherConfig=none \
    -DoptionIncludeErrorHandler=n \
    -DoptionIncludeExamples=y \
    -DoptionIncludeFrontendModule=y \
    -DpackageGroup=wknd \
    -DsiteName="WKND Site"
```


### Los parametros del arquetipo

<table class="table">

<!--cq:include script="../../common/tablestack.jsp" /-->

<colgroup><col style="width: 41%"><col style="width: 37%"><col style="width: 22%"></colgroup>
<thead class="thead">

<tr class="row"><th class="entry"><div class="p">Name

</div></th><th class="entry"><div class="p">Values

</div></th><th class="entry"><div class="p">Description

</div></th></tr>
    
</thead>


<tbody class="tbody">

<tr class="row"><td class="entry"><div class="p">groupId

</div></td><td class="entry"><div class="p">com.adobe.aem.guides

</div></td><td class="entry"><div class="p">Base Maven groupId

</div></td></tr>

<tr class="row"><td class="entry"><div class="p">artifactId

</div></td><td class="entry"><div class="p">aem-guides-wknd

</div></td><td class="entry"><div class="p">Base Maven ArtifactId

</div></td></tr>

<tr class="row"><td class="entry"><div class="p">version

</div></td><td class="entry"><div class="p">0.0.1-SNAPSHOT

</div></td><td class="entry"><div class="p">Version

</div></td></tr>

<tr class="row"><td class="entry"><div class="p">package

</div></td><td class="entry"><div class="p">com.adobe.aem.guides.wknd

</div></td><td class="entry"><div class="p">Java Source Package

</div></td></tr>

<tr class="row"><td class="entry"><div class="p">appsFolderName

</div></td><td class="entry"><div class="p">wknd

</div></td><td class="entry"><div class="p">/apps folder name

</div></td></tr>

<tr class="row"><td class="entry"><div class="p">artifactName

</div></td><td class="entry"><div class="p">WKND Sites Project

</div></td><td class="entry"><div class="p">Maven Project Name

</div></td></tr>


<tr class="row"><td class="entry"><div class="p">componentGroupName

</div></td><td class="entry"><div class="p">WKND

</div></td><td class="entry"><div class="p">AEM component group name

</div></td></tr>


<tr class="row"><td class="entry"><div class="p">contentFolderName

</div></td><td class="entry"><div class="p">wknd

</div></td><td class="entry"><div class="p">/content folder name

</div></td></tr>


<tr class="row"><td class="entry"><div class="p">confFolderName

</div></td><td class="entry"><div class="p">wknd

</div></td><td class="entry"><div class="p">/conf folder name

</div></td></tr>


<tr class="row"><td class="entry"><div class="p">cssId

</div></td><td class="entry"><div class="p">wknd

</div></td><td class="entry"><div class="p">prefix used in generated css

</div></td></tr>




<tr class="row"><td class="entry"><div class="p">packageGroup

</div></td><td class="entry"><div class="p">wknd

</div></td><td class="entry"><div class="p">Content Package Group name

</div></td></tr>













<tr class="row"><td class="entry"><div class="p">siteName

</div></td><td class="entry"><div class="p">WKND Site

</div></td><td class="entry"><div class="p">AEM site name

</div></td></tr>













<tr class="row"><td class="entry"><div class="p">optionAemVersion

</div></td><td class="entry"><div class="p">6.5.0

</div></td><td class="entry"><div class="p">Target AEM version

</div></td></tr>













<tr class="row"><td class="entry"><div class="p">language_country

</div></td><td class="entry"><div class="p">en_us

</div></td><td class="entry"><div class="p">language / country code to create the content structure from (e.g. en_us)

</div></td></tr>













<tr class="row"><td class="entry"><div class="p">optionIncludeExamples

</div></td><td class="entry"><div class="p">y

</div></td><td class="entry"><div class="p">Include a Component Library example site

</div></td></tr>













<tr class="row"><td class="entry"><div class="p">optionIncludeErrorHandler

</div></td><td class="entry"><div class="p">n

</div></td><td class="entry"><div class="p">Include a custom 404 response page

</div></td></tr>













<tr class="row"><td class="entry"><div class="p">optionIncludeFrontendModule

</div></td><td class="entry"><div class="p">y

</div></td><td class="entry"><div class="p">Include a dedicated frontend module

</div></td></tr>













<tr class="row"><td class="entry"><div class="p">isSingleCountryWebsite

</div></td><td class="entry"><div class="p">n

</div></td><td class="entry"><div class="p">Create language-master structure in example content

</div></td></tr>













<tr class="row"><td class="entry"><div class="p">optionDispatcherConfig

</div></td><td class="entry"><div class="p">none

</div></td><td class="entry"><div class="p">Generate a dispatcher configuration module

</div></td></tr>
</tbody>
 
 
  </table>
  
  
Esto crearia la siguiente estructura de directorios

```
 ~/code/
    |--- aem-guides-wknd/
        |--- all/
        |--- core/
        |--- ui.apps/
        |--- ui.content/
        |--- ui.frontend /
        |--- it.launcher/
        |--- it.tests/
        |--- pom.xml
        |--- README.md
        |--- .gitignore

```