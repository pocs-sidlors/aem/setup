## setup

Crear la siguiente estructura de carpetas:
<pre>
/Development/AEM/instances
└── author
└── publish
</pre>

En las carpetas author y publish colocar una copia de los archivos:
“Adobe capacitación à AEM à author à cq-quickstart-6.5.0.jar”
“Adobe capacitación à AEM à author à license.zip à license.properties”
Ir a la carpeta author y renombrar cq-quickstart-6.5.0.jar por aem-author-4502.jar
Ir a la carpeta publish y renombrar cq-quickstart-6.5.0.jar por aem-publish-4503.jar
La estructura de carpetas debe quedar:

<pre>
/Development/AEM/instances
├── author
│   ├── aem-author-p4502.jar
│   └── license.properties
└── publish
    ├── aem-publish-p4503.jar
    └── license.properties
</pre>

Ir a la carpeta author y dar doble clic en aem-author-p4502.jar, observarán que les abre una ventana que muestra el avance de la instalación de la instancia Author, este proceso tarda de 10 a 20 minutos dependiendo del ancho de banda y hardware del dispositivo de computo y solo es la primera vez que se inicia, en los reinicios posteriores, el proceso va con mayor agilidad. Una vez instalado, se abrirá http://localhost:4502 en un pestaña en su explorador web predeterminado.

Repetir el sexto paso pero en la carpeta publish y verificar que les abra en su navegador http://localhost:4503.

Validar que puedan ingresar a la instancia Author y Publish, el usuario por defecto es __admin__ y la contraseña es __admin__.

Listo, ya cuentan con las instancias de Author y Publish.

Para apagar las instancias, basta con dar clic en el icono  de las ventanas emergentes del paso 6:


### Recomendaciones generales:

Al iniciar las instancias, se genera una carpeta crx-quickstart al mismo nivel del JAR, favor de no borrar debido a que esta carpeta contiene la instalación y todo el contenido que vayan desarrollando.


<pre>
/Development/AEM/instances
├── author
│   ├── aem-author-p4502.jar
│   ├── crx-quickstart
│   └── license.properties
└── publish
    ├── aem-publish-p4503.jar
   	├── crx-quickstart
    └── license.properties
</pre>
Se recomienda utilizar los números pares para las instancias Author y los impares para el Publish, el estándar es usar el 4502 y 4503, en caso de que estos puertos se encuentren ocuapados por otro proceso utilizar los puertos 4504 para Author y 4505 para Publish.